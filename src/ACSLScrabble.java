import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;


public class ACSLScrabble {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
		HashMap pointValues = new HashMap(); //stores letter to point values
		pointValues.put('a', 1);		//point value initialization
		pointValues.put('e', 1);
		pointValues.put('d', 2);
		pointValues.put('r', 2);
		pointValues.put('b', 3);
		pointValues.put('m', 3);
		pointValues.put('v', 4);
		pointValues.put('y', 4);
		pointValues.put('j', 8);
		pointValues.put('x', 8); 
		
		System.out.println("ACSL Scrabble");

		String word = "";
		while (true) { //this is a user error handling loop, in case the user fails at entering a valid word
			System.out.println("Enter your word:");
			try {
				word = inputReader.readLine();
			}
			catch (IOException ie) {
				System.out.println("ERROR: IO not working properly.");
				return;
			}
			
			word = word.trim();
			word = word.toLowerCase();
			word = word.replaceAll("," , "");
			//System.out.println(word); //extra logging
			if (word.length() != 4) {
				System.out.println("ERROR: Word must be exactly 4 letters.");
				continue; //restart the loop, the user failed at entering a valid word
			}
			boolean validWord = true; //flag to see if it's a valid word
			for (int i = 0; i < 4; i++) { //loop to check if the letters in the word are letters that have point values, otherwise gives an error and restarts loop
				char currentCharacter = word.charAt(i);
				if (!pointValues.containsKey(currentCharacter)) {
					System.out.println("ERROR: You picked a letter that is not included in ACSL Scrabble.");
					validWord = false;
					break;
				}
			}
			if (validWord) {
				break;
			}
		}
		

		for(int i = 0; i<5; i++) {
			System.out.println("Starting Position "+(i+1)); //user friendly prompt
			String startingPosition = "";
			int startingNumber = 0;
			boolean isVertical = false;
			while (true) { //loop to do error checking
				try { 
					 startingPosition = inputReader.readLine();
				}
				catch (IOException ie) {
					System.out.println("ERROR: IO not working properly.");
				}
				startingPosition = startingPosition.trim();  //these lines just remove extra formatting
				startingPosition = startingPosition.toLowerCase();
				startingPosition = startingPosition.replaceAll(" ", "");
				startingPosition = startingPosition.replaceAll(",", "");
				//System.out.println("Starting Position deformatted = "+startingPosition); //extra logging
				if (startingPosition.length() == 0) {
					System.out.println("ERROR: Please enter a value.");
					continue; //restart the error loop;
				}
				try { //get the starting index
					startingNumber = Integer.parseInt(startingPosition.substring(0, startingPosition.length()-1)); //get the number, this is all the characters of the string except the last.
				}
				catch (NumberFormatException nfe) {
					System.out.println("ERROR: This is not a valid starting position. Try again.");
					continue;
				}
				char c = startingPosition.charAt(startingPosition.length()-1); //get the last character of the string, which tells us the direction of the word.
				if (c == 'h') {
					isVertical = false; //not strictly necessary, but added for convenience
				}
				else if (c == 'v') {
					isVertical = true;
				}
				else { //default case in case there's a bad input
					System.out.println("ERROR: This is not a valid starting position. Try again.");
					continue; //we would like to loop again and get a valid value
				}	
				
				if (startingNumber < 1 || startingNumber > 40) { //check to see if the number is in the range
					System.out.println("ERROR: The starting index is out of range. Try again.");
					continue;
				}
				if (startingNumber % 10 > 7 && isVertical == false) { //this modulus checks to see if the word would run off the board to the right
					System.out.println("ERROR: With that starting position, the word would run off the board. Try again.");
					continue;
				}
				if (startingNumber > 10 && isVertical == true) { //this one checks if the word would run off the bottom
					System.out.println("ERROR: With that starting position, the word would run off the board. Try again.");
					continue;
				}
				break;
			}
			//System.out.println("Starting index: "+startingNumber); //extra log details.
			//System.out.println("Vertical? "+isVertical);
			boolean doubleWordScore = false, tripleWordScore = false;
			int pointsSum = 0;
			if (!isVertical) { //horizontal code goes here
				for (int j = 0; j < 4; j++) {
					boolean doubleLetterScore = false, tripleLetterScore = false;
					char currentChar = word.charAt(j);
					int currentNumber = startingNumber+j;//this gives us the current square
					if (currentNumber % 3 == 0 && currentNumber % 6 != 0) {//Double Letter - every other multiple of 3, so basically multiples of 3 and not 6
						doubleLetterScore = true;
					}
					else if (currentNumber % 5 == 0) { //this is an else, so if the if above is true, this won't happen
						tripleLetterScore = true;
					}
					else if (currentNumber % 7 == 0) { //else, so won't happen if above is true
						doubleWordScore = true;
					}
					else if (currentNumber % 8 == 0) {
						tripleWordScore = true;
					}
					int rawPoints = (int) pointValues.get(currentChar);//get the raw point values
					if (doubleLetterScore) {
						pointsSum+=rawPoints*2; //add double the letter 
					}
					else if (tripleLetterScore) {
						pointsSum+=rawPoints*3; //triple the letter
					}
					else {
						pointsSum+=rawPoints; //under just a normal square, or a double or triple word square.
					}
				}
			}
			else { //vertical code goes here
				for (int j = 0; j < 4; j++) {
					boolean doubleLetterScore = false, tripleLetterScore = false;
					char currentChar = word.charAt(j);
					int currentNumber = startingNumber+10*j;//this gives us the current square, note the change for vertical
					if (currentNumber % 3 == 0 && currentNumber % 6 != 0) {//Double Letter - every other multiple of 3, so basically multiples of 3 and not 6
						doubleLetterScore = true;
					}
					else if (currentNumber % 5 == 0) { //this is an else, so if the if above is true, this won't happen
						tripleLetterScore = true;
					}
					else if (currentNumber % 7 == 0) { //else, so won't happen if above is true
						doubleWordScore = true;
					}
					else if (currentNumber % 8 == 0) {
						tripleWordScore = true;
					}
					int rawPoints = (int) pointValues.get(currentChar);//get the raw point values
					if (doubleLetterScore) {
						pointsSum+=rawPoints*2; //add double the letter 
					}
					else if (tripleLetterScore) {
						pointsSum+=rawPoints*3; //triple the letter
					}
					else {
						pointsSum+=rawPoints; //under just a normal square, or a double or triple word square.
					}
				}
			}
			if (tripleWordScore) { //you can only have one score multiplier at a time, so default to the higher one
				pointsSum*=3;
			}
			else if (doubleWordScore) {
				pointsSum*=2;
			}
			System.out.println("Points: "+pointsSum); //print the answer
			
		}
		
	}

}
